﻿using COPMDU.Domain.Entity;
using COPMDU.Infrastructure.ClassAttribute;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace COPMDU.Domain
{
    public class OutageResponse
    {
        [PageResultProperty(@"class=""ticket_value"">(\d+)</td>", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int Id { get; set; }

        [PageResultProperty(@"Categoria:<\/b><\/td><td class=""ticket_value"">(\w+?) <em>", Active = false)]
        public string Category { get; set; }

        [PageResultProperty(@"/Categoria:<\/b><\/td><td class=""ticket_value"">.+?<em>\((.+?) subcaso\(s\)\)<\/em>/gi", ValueType = PageResultPropertyAttribute.ConversionType.Integer, Active = false)]
        public int SubCategoryCount { get; set; }

        [PageResultProperty(@"Tipo:<\/b><\/td><td class=""ticket_value"">(\w+?)<\/td>")]
        public string Type { get; set; }

        [PageResultProperty(@"os Afetados:<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>")]
        public string AffectedServices { get; set; }

        [PageResultProperty(@"Cidade<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>")]
        public string City { get; set; }

        [PageResultProperty(@"Data:<\/b><\/td><td class=""ticket_value"">(\d{2}\/\d{2}\/\d{4}\s\d{2}:\d{2})<\/td>", ValueType = PageResultPropertyAttribute.ConversionType.Date)]
        public DateTime Date { get; set; }

        [PageResultProperty(@"Fila:<\/b><\/td><td class=""ticket_value"">(.+?)\s\(Usu&aacute;rio.+?<\/em>\)<\/td>")]
        [PageResultProperty(@"Fila:<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>")]
        public string Queue { get; set; }

        [PageResultProperty(@"<form name=""frmTit""><input type=""text"" class=""campotexto"" name=""txtTitulo"" size=""60"" maxlength=""65"" value=""(.+?)"">")]
        [PageResultProperty(@"T&iacute;tulo:<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>")]
        public string Title { get; set; }

        [PageResultProperty(@"Descri&ccedil;&atilde;o:<\/b><\/td><td class=""ticket_value"">([\S\s]+?)<\/td>")]
        public string Description { get; set; }

        [PageResultProperty(@"Status:<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>")]
        public string Status { get; set; }

        [PageResultProperty(@"Aberto por:<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>")]
        public string OpenedBy { get; set; }

        [PageResultProperty(@"Data Inicio:<\/b><\/td><td class=""ticket_value"">(\d{2}\/\d{2}\/\d{4}\s\d{2}:\d{2})<\/td>", ValueType = PageResultPropertyAttribute.ConversionType.Date)]
        public DateTime StartDate { get; set; }

        [PageResultProperty(@"Data Final:<\/b><\/td><td class=""ticket_value"">(\d{2}\/\d{2}\/\d{4}\s\d{2}:\d{2})<\/td>")]
        public string FinalDate { get; set; }

        [PageResultProperty(@"<b>Local:</b> \d{8,9}[\s\-]*(.+?)\(C&Oacute;D.+?</font>")]
        [PageResultProperty(@"<b>Local:</b> (.+?)\/\/.+?</font>")]
        [PageResultProperty(@"<b>Local:</b> (.+?)\(C&Oacute;D.+?</font>")]
        public string Address { get; set; }

        [PageResultProperty(@"<span id=""lstNodesAfetados"" name=""lstNodesAfetados"">(\w+?)<\/span>")]
        public string Node { get; set; }

        [PageResultProperty(@"C&eacute;lula<\/td><\/tr><tr><td class=""ticket_value"">(.+?)<\/td>", Active = false)]
        public string District { get; set; }

        [PageResultProperty(@"Sintoma:<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>")]
        public string Symptom { get; set; }

        [PageResultProperty(@"Natureza:<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>")]
        public string Nature { get; set; }

        [PageResultProperty(@"Manobra n&ordm;:<\/b><\/td><td class=""ticket_value""><em>(.+?)</em><\/td>", Active = false)]
        public string Maneuver { get; set; }

        [PageResultProperty(@"Na URA?<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>", ValueType = PageResultPropertyAttribute.ConversionType.Boolean, Active = false)]
        public bool InUra { get; set; }

        [PageResultProperty(@"Na Central?<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>", ValueType = PageResultPropertyAttribute.ConversionType.Boolean, Active = false)]
        public bool InCentral { get; set; }

        [PageResultProperty(@"No ATLAS?<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>", ValueType = PageResultPropertyAttribute.ConversionType.Boolean, Active = false)]
        public bool InAtlas{ get; set; }

        [PageResultProperty(@"rea de risco?<\/b><\/td><td class=""ticket_value"">(.+?)<\/td>", ValueType = PageResultPropertyAttribute.ConversionType.Boolean, Active = false)]
        public bool Risk { get; set; }

        [PageResultProperty(@"CONTRATO.+?\s*(\d*-?\d)", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        [PageResultProperty(@"Descri&ccedil;&atilde;o:<\/b><\/td><td class=""ticket_value"">[\S\s]+?(\d{8}-\d)[\S\s]+?<\/td>", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        [PageResultProperty(@"Descri&ccedil;&atilde;o:<\/b><\/td><td class=""ticket_value"">[\S\s]+?(\d{9})[\S\s]+?<\/td>", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int Contract { get; set; }

        [PageResultProperty(@"NOTIFICAÇÃO\:?\s*(\d+?)\s", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        [PageResultProperty(@"NOTIFICA.+?(\d{6})", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        [PageResultProperty(@"ATLAS.+?(\d{6})", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        [PageResultProperty(@"NOT.+?(\d{6})", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int Notification { get; set; }

        [PageResultProperty(@"[Gg][Ee][Dd]:\s?(\d{9})", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        [PageResultProperty(@"D MDU:\s?(\d{9})", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int IdMdu { get; set; }

        public ServiceOrder ServiceOrder { get; set; }
    }
}
